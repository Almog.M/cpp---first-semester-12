#include "threads.h"
#include <thread>
#include <mutex>

std::mutex mu;


void I_Love_Threads()
{
	std::cout << "I love threads.\n";
}

void call_I_Love_Threads()
{
	std::thread t(I_Love_Threads);
	t.join();
}

void printVector(vector<int> v)
{
	int sizeOfVector = (int)v.size();
	for (int i = 0; i < sizeOfVector; i++)
	{
		std::cout << v[i] << "\n";
	}
	std::cout << "\n";
}

void getPrimes(int begin, int end, vector<int>& primes)
{
	bool isPrime = true; //As default - NOT FORGER TO CHANGE ANY ITERATION - for each number.

	if (end < begin)
		cout << "The end can not be smaller than the begin...\n";

	else if (begin >= 0 && end >= 0)
	{
		for (int num = begin; num <= end; num++)
		{
			if (num > 1) //We can not do some math on 0 in this case...And we know that 1 is an exception - is not prime.
			{
				isPrime = true;
				for (int runner = 2; runner <= sqrt(num) && isPrime; runner++) //Runner runs on the current number.
				{
					if (num % runner == 0) //If the number is not prime...
					{
						isPrime = false;
					}
				}
				if (isPrime) //Checks if the current number is prime... 
				{
					primes.push_back(num);
				}
			}
		}
	}
	else
		cout << "Negative numbers can not be primes...\n";
}

vector<int> callGetPrimes(int begin, int end)
{
	vector<int> primes;
	//Clicks of the thread:
	clock_t clicks; //We need to use here in clock() for calculate the time.
	clicks = clock();
	std::thread t(getPrimes, begin, end, std::ref(primes));
	t.join();
	std::cout << "The time of the get primes thread is: " << (double)(clock() - clicks) / 1000 << " seconds\n\n";

	return primes;
}

void writePrimesToFile(int begin, int end, ofstream & file)
{
	vector<int> primes = callGetPrimes(begin, end);                                       
	int i = 0;

	for (i = 0; i < (int)primes.size(); i++)
	{
		mu.lock();
		file << primes[i] << " \n";
		mu.unlock();
	}
}

void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	clock_t timeBefore;
	std::thread* threads = new std::thread [N];
	int genaralRange = (end - begin + 1); //Of the parameters that has sent.
	int rangeSize = genaralRange / N; //For each thread (maybe not for the last - dependents on curry).
	ofstream file(filePath);
	int start; //Of each thread.
	int finish = begin - 1; //Of each thread.

	timeBefore = clock();
	for (int i = 0; i < N; i++)
	{
		start = finish + 1; //Of each thread.
		if (i == N - 1) finish = end; //Last iteration...
		else finish = start + rangeSize;
		threads[i] = std::thread(writePrimesToFile, start, finish ,std::ref(file));
	}
	   	  
	for (int i = 0; i < N; i++)
		threads[i].join();
	

	std::cout << "The time of the N threads (N = " << N << ") in the callWritePrimesMultipleThreads function is: " << (double)(clock() - timeBefore) / 1000 << " seconds\n\n";
	delete[] threads;
	file.close();
}

