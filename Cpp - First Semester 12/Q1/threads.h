#include <string>
#include <fstream>
#include <iostream>
#include<vector>
#include <thread>
#include <exception>
#include <time.h>
#include "Math.h"
#include <stdlib.h>
#include <vector>


using namespace std;


void I_Love_Threads(); //Prints the name of the func.
void call_I_Love_Threads(); //Call to the "I love..." function with thread.

void printVector(vector<int> v);

void getPrimes(int begin, int end, vector<int>& primes);
vector<int> callGetPrimes(int begin, int end);


void writePrimesToFile(int begin, int end, ofstream& file);
void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N);
