#include "MessageSender.h"





MessageSender::MessageSender()
{
	this->_usersSet = {};
}


MessageSender::~MessageSender()
{
}

void MessageSender::printMenu()
{
	std::cout << "1 --> Sign in.\n";
	std::cout << "2 --> Sign out.\n";
	std::cout << "3 --> Connected Users.\n";
	std::cout << "4 --> Exit.\n\n";
}

void MessageSender::signIn()
{
	string userName;
	std::cout << "Enter a name: \n";
	userName = this->getString();

	//If the name is not in the set...
	if (this->_usersSet.find(userName) == this->_usersSet.end()) this->_usersSet.insert(userName);
	else std::cout << "The user: " << userName << " is already in the users set...\n";
}

void MessageSender::signOut()
{
	string userName;
	std::cout << "Enter a name: \n";
	userName = this->getString();
	
	//If the name is in the set...
	if (this->_usersSet.find(userName) != this->_usersSet.end()) this->_usersSet.erase(userName);
	else std::cout <<  "The user: " << userName << " is not existed in the users set...\n";
}

void MessageSender::connectedUsers()
{
	std::set<string>::iterator it = this->_usersSet.begin();
	std::set<string>::iterator endOf = this->_usersSet.end();

	if (it != endOf)
	{
		while (it != endOf)
		{
			std::cout << *it << "\n";
			it++;
		}
	}
	else
		std::cout << "The users set is empty...\n\n";
}

string MessageSender::getString()
{
	string str;
	std::cin >> str;
	getchar(); //Cleaning buffer.


	return str;
}

queue<string> MessageSender::parseData(fstream & dataFile)
{
	queue<string> dataAsQueue;
	long int fileSize;
	char* buffer = nullptr;

	//If the file is not opened...
	if (!dataFile.is_open()) dataFile.open("data.txt");
	
	if (!dataFile.fail()) //If the opening has succeeded...
	{
		while (true)
		{
			fileSize = dataFile.tellg();
			if (fileSize == 0) std::cout << "The file is empty...\n\n"; //The file is empty...
			else
			{
				buffer = (char*)realloc(buffer, fileSize + 1);
				dataFile.read(buffer, fileSize);
				buffer[fileSize] = 0;
				this->parseBufferToQueue(dataAsQueue, buffer, '\n');
				this->printQueue(dataAsQueue); //For debugging...
			}
			Sleep(60000);
		}
	}
	else
		std::cout << "The file is corrupt...\n\n";
	
	dataFile.close();
	delete[] buffer;
	return dataAsQueue;
}

void MessageSender::parseBufferToQueue(queue<string>& q, char * buffer, char parseSign)
{
	char ch = ' ';
	int i = -1;
	string item;
	while (ch != 0)
	{
		if (i != -1)
		{
			if (ch != parseSign) item += ch;
			else if (ch == parseSign || buffer[i + 1] == 0) //If the sign is the parse sign or the char is the last char...
			{
				q.push(item);
				item = "";
			}
		}

		i++;
		ch = buffer[i];
	}
}

void MessageSender::printQueue(queue<string> q)
{
	while (!q.empty())
	{
		std::cout << q.front() << "\n";
		q.pop();
	}
	

}
