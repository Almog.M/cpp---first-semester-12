#pragma once
#include <iostream>
#include <set>
#include <string>
#include <fstream>
#include <queue>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>


#define SLEEP_TIME 60000 //For the parse function...

using std::string;
using std::fstream;
using std::queue;

class MessageSender
{
private:
	std::set<string> _usersSet;
public:
	MessageSender();
	~MessageSender();
	void printMenu();
	void signIn();
	void signOut();
	void connectedUsers(); //Prints the names of the all existed users.
	string getString();
	
	
	//Getters:
	std::set<string>& getUsersSet() { return this->_usersSet; }

	//Setters
	void setUsersSet(std::set<string> usersSet) { this->_usersSet = usersSet; }

	 queue<string> parseData(fstream& dataFile); //Parse data from file. Returns the data as queue of strings.
	 void parseBufferToQueue(queue<string>& q, char* buffer, char parseSign);
	 void printQueue(queue<string> q);
	
};

