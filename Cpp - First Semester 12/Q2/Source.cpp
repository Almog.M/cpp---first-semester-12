#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "MessageSender.h"
#include <string>
#include <fstream>

using std::string;

int main()
{
	int ans;
	MessageSender ms;
	string name;
	fstream file("data.txt");
	do
	{
		ms.printMenu();
		std::cin >> ans;
		getchar();
		switch (ans)
		{
		case 1:
			ms.signIn();
			break;

		case 2:
			ms.signOut();
			break;

		case 3:
			ms.connectedUsers();
			break;

		case 4:
			std::cout << "Bye Bye....\n";
			break;

		case 5:
			ms.parseData(file);
			break;
		default:
			std::cout << "Invalid answer...\n";
			break;
		}
	} while (ans != 4);


	getchar();
	return 0;
}